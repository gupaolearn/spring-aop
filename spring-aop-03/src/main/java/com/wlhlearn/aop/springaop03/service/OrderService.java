package com.wlhlearn.aop.springaop03.service;

import org.springframework.stereotype.Service;

/**
 * @Author: wlh
 * @Date: 2019/4/10 15:48
 * @Version 1.0
 * @despricate:learn
 */

@Service
public class OrderService {

    public  void  insertOrder(){
        System.out.println("insert order");
    }
}
