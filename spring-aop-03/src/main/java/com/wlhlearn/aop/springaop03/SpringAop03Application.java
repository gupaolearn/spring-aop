package com.wlhlearn.aop.springaop03;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringAop03Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringAop03Application.class, args);
	}

}
