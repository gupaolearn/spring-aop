package com.wlhlearn.aop.springaop03.aop;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * @Author: wlh
 * @Date: 2019/4/10 15:49
 * @Version 1.0
 * @despricate:learn
 */

@Component
@Aspect

public class Annotationaop {

    @Pointcut("execution(* com.wlhlearn.aop.springaop03.service.OrderService..*(..))")
    public  void  aspect(){

    }

   @Before("aspect()")
    public  void  before(){
        System.out.println("before  method,  we  try  to  do something!!!! ");
    }

}
