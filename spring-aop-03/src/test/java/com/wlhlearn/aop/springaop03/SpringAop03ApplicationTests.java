package com.wlhlearn.aop.springaop03;

import com.wlhlearn.aop.springaop03.aop.Annotationaop;
import com.wlhlearn.aop.springaop03.service.OrderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringAop03ApplicationTests {

	@Autowired
	OrderService orderService ;

	@Autowired
	ApplicationContext applicationContext ;

	@Test
	public  void  before(){
		Annotationaop annotationaop=applicationContext.getBean(Annotationaop.class);
		orderService.insertOrder();
		System.out.println("1111111");
	}

}
